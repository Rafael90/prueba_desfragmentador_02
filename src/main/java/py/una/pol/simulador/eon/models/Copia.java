/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.una.pol.simulador.eon.models;
import lombok.Data;
import org.jgrapht.Graph;
/**
 *
 * @author Rafa
 */

@Data
public class Copia {
    
        /**
     * Para copiar un grafo
     */
    private Graph<Integer, Link> graph;

    /**
     * Constructor vacío
     */
    public Copia() {
    }
        /**
     * Constructor con parámetros
     *
     * @param graph Topología de la red
     */
     public Copia(Graph<Integer, Link> graph) {
        this.graph = graph;
    }
}
